#!/usr/bin/python3

from which_pyqt import PYQT_VER
if PYQT_VER == 'PYQT5':
	from PyQt5.QtCore import QLineF, QPointF
elif PYQT_VER == 'PYQT4':
	from PyQt4.QtCore import QLineF, QPointF
else:
	raise Exception('Unsupported Version of PyQt: {}'.format(PYQT_VER))

import math
import time

# Used to compute the bandwidth for banded version
MAXINDELS = 3

# Used to implement Needleman-Wunsch scoring
MATCH = -3
INDEL = 5
SUB = 1

ORIGIN = -1
DIAGONAL = 0
VERTICAL = 1
HORIZONTAL = 2

GAP_CHARACTER = '-'

class GeneSequencing:

	def __init__( self ):
		pass

	
# This is the method called by the GUI.  _sequences_ is a list of the ten sequences, _table_ is a
# handle to the GUI so it can be updated as you find results, _banded_ is a boolean that tells
# you whether you should compute a banded alignment or full alignment, and _align_length_ tells you 
# how many base pairs to use in computing the alignment

	def align( self, sequences, table, banded, align_length):
		self.banded = banded
		self.MaxCharactersToAlign = align_length
		results = []

		# Go through every sequence we are given
		for i in range(len(sequences)):

			# Store the results of each comparison in a list
			jresults = []

			# Compare the sequence to every other sequence
			for j in range(len(sequences)):

				# Don't compare any sequence to one that it has been compared to before
				if(j < i):
					s = {}
				else:
###################################################################################################
					# Get the sequences to be aligned. Constant time and space complexity.
					first_sequence = sequences[i][:self.MaxCharactersToAlign]
					second_sequence = sequences[j][:self.MaxCharactersToAlign]

					# For the banded algorithm, time and space complexity is O(km), where k is the banded width and m is the length of the shorter sequence
					score, alignment1, alignment2 = None, None, None
					if banded:
						score, alignment1, alignment2 = self.bandedAlign(first_sequence, second_sequence)
					# For the unbanded algorithm, time and space complexity is O(nm), where n is the length of the first sequence and m is the length of the second sequence
					else:
						score, alignment1, alignment2 = self.unbandedAlign(first_sequence, second_sequence)
###################################################################################################					
					s = {'align_cost':score, 'seqi_first100':alignment1, 'seqj_first100':alignment2}
					table.item(i,j).setText('{}'.format(int(score) if score != math.inf else score))
					table.repaint()	
				jresults.append(s)
			results.append(jresults)
		return results

	def unbandedAlign(self, first_sequence, second_sequence):
		# Initialize the dynamic table. Constant time and space complexity.
		dynamic_table = []

		# Go through the alignment. 
		# This runs n + 1 times, where n is the length of the first sequence. O(n) complexity multiplied by the inner loop complexity of O(m). Total of O(nm) complexity.
		for first_sequence_index in range(-1, len(first_sequence)):

			# Increment the table row index
			table_row_index = first_sequence_index + 1

			# Start out the column index so we can increment only when necessary
			table_column_index = -1
			
			# Compare the characters in the second sequence to the characters in the first sequence. 
			# Runs m + 1 times, where m is the length of the second sequence. O(m) complexity
			for second_sequence_index in range(-1, len(second_sequence)):

				# Increment the column index for our dynamic table
				table_column_index += 1

				# If we are at the origin of the table, then put into the table the origin value of nothing, indicating with the backpointer that it is the origin, then go to the next loop iteration
				if table_row_index == 0 and table_column_index == 0:
					dynamic_table.append([(0, ORIGIN)])
					continue

				# Find the smallest score for this index in the table. Included in the value pair is the backpointer to show where the score came from. Constant time and space complexity
				smallest_value_pair = self.getBestValuePairUnbanded(first_sequence, second_sequence, first_sequence_index, second_sequence_index, table_row_index, table_column_index, dynamic_table)

				# Append that value to our dynamic table. Constant time and space complexity. This might not always be constant, depending on how python deals internally with arrays.
				dynamic_table[table_row_index].append(smallest_value_pair)

			# Add another row to the dynamic table
			dynamic_table.append([])

		# Take out the extra row we added at the end.. Constant time and space complexity. Although perhaps python might make it be longer time complexity, depending on how it deals internally with arrays
		dynamic_table.pop()

		# Based on the dynamic matrix, find the optimal score and alignments for the two sequences. O(n) time complexity, where n is the length of the longer sequence
		score, alignment1, alignment2 = self.getAlignmentsUnbanded(first_sequence, second_sequence, dynamic_table)
		
		return score, alignment1, alignment2

	def bandedAlign(self, first_sequence, second_sequence):
		# If there cannot be any possible alignment, give up and report that
		if self.banded and abs(len(first_sequence) - len(second_sequence)) > MAXINDELS:
			return math.inf, "No Alignment Possible", "No Alignment Possible"

		# Initialize the dynamic table. Constant time and space complexity.
		dynamic_table = []

		# Go through the alignment. 
		# This runs m + 1 times, where m is the length of the shorter sequence. O(m) complexity multiplied by the inner loop complexity of O(k). Total of O(km) complexity.
		for first_sequence_index in range(-1, len(first_sequence)):

			# Increment the table row index
			table_row_index = first_sequence_index + 1
			
			# Find out how many iterations we should go through (the width on this row for the dynamic table)
			banded_width = (MAXINDELS * 2) + 1
			if table_row_index <= MAXINDELS:
				banded_width = MAXINDELS + 1 + table_row_index

			# Compare the characters in the second sequence to the characters in the first sequence. 
			# Runs k times, where k is the bandwidth). O(k) complexity.
			for table_column_index in range(0, banded_width):

				# Find the offset for the second sequence index
				column_offset = table_row_index - MAXINDELS
				if table_row_index <= MAXINDELS:
					column_offset = 0

				# Find the second sequence index based on that offset
				second_sequence_index = table_column_index + column_offset - 1

				if second_sequence_index >= len(second_sequence):
					break

				# If we are at the origin of the table, then put into the table the origin value of nothing, indicating with the backpointer that it is the origin, then go to the next loop iteration
				if table_row_index == 0 and table_column_index == 0:
					dynamic_table.append([(0, ORIGIN)])
					continue

				# Find the smallest score for this index in the table. Included in the value pair is the backpointer to show where the score came from. Constant time and space complexity
				smallest_value_pair = self.getBestValuePairBanded(first_sequence, second_sequence, first_sequence_index, second_sequence_index, table_row_index, table_column_index, dynamic_table)

				# Append that value to our dynamic table. . Constant time and space complexity. This might not always be constant, depending on how python deals internally with arrays.
				dynamic_table[table_row_index].append(smallest_value_pair)

			# Add another row to the dynamic table
			dynamic_table.append([])

		# Take out the extra row we added at the end.. Constant time and space complexity. Although perhaps python might make it be longer time complexity, depending on how it deals internally with arrays
		dynamic_table.pop()

		# Based on the dynamic matrix, find the optimal score and alignments for the two sequences. O(n) time complexity, where n is the length of the longer sequence
		score, alignment1, alignment2 = self.getAlignmentsBanded(first_sequence, second_sequence, dynamic_table)

		return score, alignment1, alignment2

	# Constant time and space complexity
	def getBestValuePairUnbanded(self, first_sequence, second_sequence, first_sequence_index, second_sequence_index, table_row_index, table_column_index, dynamic_table):
		# Get the diagonal value. If there is no diagonal, then consider its value to be infinite
		if table_row_index == 0 or table_column_index == 0:
			diagonal_value = math.inf
		elif first_sequence[first_sequence_index] == second_sequence[second_sequence_index]:
			diagonal_value = dynamic_table[table_row_index - 1][table_column_index - 1][0] + MATCH
		else:
			diagonal_value = dynamic_table[table_row_index - 1][table_column_index - 1][0] + SUB
	
		# Get the horizontal gap value. If there is no horizontal, then consider its value to be infinite
		if table_column_index == 0:
			horizontal_value = math.inf
		else:
			horizontal_value = dynamic_table[table_row_index][table_column_index - 1][0] + INDEL
	
		# Get the vertical gap value. If there is no vertical, then consider its value to be infinite
		if table_row_index == 0:
			vertical_value = math.inf
		else:
			vertical_value = dynamic_table[table_row_index - 1][table_column_index][0] + INDEL
		
		# Find the smallest value, whether from diagonal, horizontal, or vertical alignment
		smallest_value_pair = diagonal_value, DIAGONAL
		if smallest_value_pair[0] > horizontal_value:
			smallest_value_pair = horizontal_value, HORIZONTAL
		if smallest_value_pair[0] > vertical_value:
			smallest_value_pair = vertical_value, VERTICAL

		return smallest_value_pair

	# O(n) time complexity, where n is the length of the longer sequence
	def getAlignmentsUnbanded(self, first_sequence, second_sequence, dynamic_table):
		# Get the last element in the matrix
		current_element = dynamic_table[-1][-1]
		
		# Find the index of that element
		current_row_index = len(dynamic_table) - 1
		current_column_index = len(dynamic_table[-1]) - 1
		
		# Get the score for the optimal alignment
		score = current_element[0]
		
		# Initialize the alignment strings
		alignment1_string = ""
		alignment2_string = ""
		
		# Go back through the dynamic table (matrix) using the backpointer. The time complexity is O(n), where n is the length of the longer sequence
		while current_element[1] != ORIGIN:
			if current_element[1] == HORIZONTAL:
				current_column_index -= 1
				alignment1_string = GAP_CHARACTER + alignment1_string
				alignment2_string =  second_sequence[current_column_index] + alignment2_string
			elif current_element[1] == VERTICAL:
				current_row_index -= 1
				alignment1_string = first_sequence[current_row_index] + alignment1_string
				alignment2_string = GAP_CHARACTER + alignment2_string
			else:
				current_row_index -= 1
				current_column_index -= 1
				alignment1_string = first_sequence[current_row_index] + alignment1_string
				alignment2_string = second_sequence[current_column_index] + alignment2_string
			current_element = dynamic_table[current_row_index][current_column_index]
		
		# Only save the first 100 values in the alignment strings
		alignment1 = alignment1_string[:100]
		alignment2 = alignment2_string[:100]

		return score, alignment1, alignment2

	# Constant time and space complexity
	def getBestValuePairBanded(self, first_sequence, second_sequence, first_sequence_index, second_sequence_index, table_row_index, table_column_index, dynamic_table):		
		# Get the column offset
		column_offset = 1
		if table_row_index <= MAXINDELS:
			column_offset = 0

		# Get the diagonal value. If there is no diagonal, then consider its value to be infinite
		if table_row_index == 0 or table_column_index + column_offset == 0:
			diagonal_value = math.inf
		elif first_sequence[first_sequence_index] == second_sequence[second_sequence_index]:
			diagonal_value = dynamic_table[table_row_index - 1][table_column_index + column_offset - 1][0] + MATCH
		else:
			diagonal_value = dynamic_table[table_row_index - 1][table_column_index + column_offset - 1][0] + SUB
	
		# Get the horizontal gap value. If there is no horizontal, then consider its value to be infinite
		if table_column_index == 0:
			horizontal_value = math.inf
		else:
			horizontal_value = dynamic_table[table_row_index][table_column_index - 1][0] + INDEL
	
		# Get the vertical gap value. If there is no vertical, then consider its value to be infinite
		if table_row_index == 0 or len(dynamic_table[table_row_index - 1]) <= table_column_index + column_offset:
			vertical_value = math.inf
		else:
			vertical_value = dynamic_table[table_row_index - 1][table_column_index + column_offset][0] + INDEL
		
		# Find the smallest value, whether from diagonal, horizontal, or vertical alignment
		smallest_value_pair = diagonal_value, DIAGONAL
		if smallest_value_pair[0] > horizontal_value:
			smallest_value_pair = horizontal_value, HORIZONTAL
		if smallest_value_pair[0] > vertical_value:
			smallest_value_pair = vertical_value, VERTICAL

		return smallest_value_pair

	# O(n) time complexity, where n is the length of the longer sequence
	def getAlignmentsBanded(self, first_sequence, second_sequence, dynamic_table):
		# Get the last element in the matrix
		current_element = dynamic_table[-1][-1]
		
		# Find the index of that element
		current_row_index = len(dynamic_table) - 1
		current_column_index = len(dynamic_table[-1]) - 1

		# Get the score for the optimal alignment
		score = current_element[0]

		# Initialize the alignment strings
		alignment1_string = ""
		alignment2_string = ""
		
		# Find the length of the second sequence so we can keep track of the real index into that sequence
		second_sequence_index = len(second_sequence)
		
		# If we are only aligning a certain amount of characters, then use that number as the beginning index into the second sequence
		if self.MaxCharactersToAlign < second_sequence_index:
			second_sequence_index = self.MaxCharactersToAlign

		# Go back through the dynamic table (matrix) using the backpointer. The time complexity is O(n), where n is the length of the longer sequence
		while current_element[1] != ORIGIN:
			# Get the column offset
			column_offset = 0
			if current_row_index <= MAXINDELS:
				column_offset = 1

			if current_element[1] == HORIZONTAL:
				current_column_index -= 1
				second_sequence_index -= 1
				alignment1_string = GAP_CHARACTER + alignment1_string
				alignment2_string =  second_sequence[second_sequence_index] + alignment2_string
			elif current_element[1] == VERTICAL:
				current_row_index -= 1
				current_column_index += 1 - column_offset
				alignment1_string = first_sequence[current_row_index] + alignment1_string
				alignment2_string = GAP_CHARACTER + alignment2_string
			else:
				current_row_index -= 1
				current_column_index -= column_offset
				second_sequence_index -= 1
				alignment1_string = first_sequence[current_row_index] + alignment1_string
				alignment2_string = second_sequence[second_sequence_index] + alignment2_string
			current_element = dynamic_table[current_row_index][current_column_index]

		# Only save the first 100 values in the alignment strings
		alignment1 = alignment1_string[:100]
		alignment2 = alignment2_string[:100]

		return score, alignment1, alignment2